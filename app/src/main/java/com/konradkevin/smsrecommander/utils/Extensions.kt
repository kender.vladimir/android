package com.konradkevin.smsrecommander.utils

import android.app.Activity
import androidx.fragment.app.Fragment
import com.konradkevin.smsrecommander.di.components.DaggerComponentProvider


val Activity.injector get() = (application as DaggerComponentProvider).component
val Fragment.injector get() = (activity?.application as DaggerComponentProvider).component
