package com.konradkevin.smsrecommander.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.konradkevin.smsrecommander.database.entities.RecommendationEntity

@Dao
interface RecommendationDao: BaseDao<RecommendationEntity> {
    @Query("SELECT * FROM recommendations WHERE mot2 IS NULL AND mot3 IS NULL ORDER BY is_starter DESC, counter DESC LIMIT :limit")
    fun getStarterWords(limit: Int): LiveData<List<RecommendationEntity>>

    @Query("SELECT * FROM recommendations WHERE mot2 IS NULL AND mot3 IS NULL ORDER BY counter DESC LIMIT :limit")
    fun getMostFrequentWords(limit: Int): LiveData<List<RecommendationEntity>>

    @Query("SELECT DISTINCT mot3 as mot1, 1 as id, NULL as mot2, NULL as mot3, COUNT(*) as counter, MAX(is_idiomatic) as is_idiomatic, MAX(is_starter) as is_starter, NULL as mode FROM recommendations WHERE mot1 = :word1 AND mot2 = :word2 AND mot3 IS NOT NULL GROUP BY mot3 ORDER BY counter DESC LIMIT :limit")
    fun get3GramsWords(word1: String, word2: String, limit: Int): LiveData<List<RecommendationEntity>>

    @Query("SELECT DISTINCT mot2 as mot1, 1 as id, NULL as mot2, NULL as mot3, COUNT(*) as counter, MAX(is_idiomatic) as is_idiomatic, MAX(is_starter) as is_starter, NULL as mode FROM recommendations WHERE mot1 = :word1 AND mot2 IS NOT NULL GROUP BY mot2 ORDER BY counter DESC LIMIT :limit")
    fun get2GramsWords(word1: String, limit: Int): LiveData<List<RecommendationEntity>>

    @Query("SELECT DISTINCT mot1 as mot1, 1 as id, NULL as mot2, NULL as mot3, COUNT(*) as counter, MAX(is_idiomatic) as is_idiomatic, MAX(is_starter) as is_starter, NULL as mode FROM recommendations WHERE mot1 LIKE :word1 GROUP BY mot1 ORDER BY counter DESC LIMIT :limit")
    fun get1GramsWords(word1: String?, limit: Int): LiveData<List<RecommendationEntity>>


    @Query("SELECT * FROM recommendations WHERE mot1 = :word1 AND mot2 IS NULL AND mot3 IS NULL")
    fun getOne(word1: String): RecommendationEntity
}