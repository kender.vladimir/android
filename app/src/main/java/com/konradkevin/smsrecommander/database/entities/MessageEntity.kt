package com.konradkevin.smsrecommander.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "messages")
data class MessageEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int?,

    @ColumnInfo(name = "content")
    val content: String,

    @ColumnInfo(name = "date")
    val date: Long,

    @ColumnInfo(name = "contactId")
    val contactId: Int,

    @ColumnInfo(name = "isItMe")
    val isItMe: Boolean = false
)