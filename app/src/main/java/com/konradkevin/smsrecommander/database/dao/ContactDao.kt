package com.konradkevin.smsrecommander.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.konradkevin.smsrecommander.database.entities.ContactEntity

@Dao
interface ContactDao: BaseDao<ContactEntity> {
    @Query("SELECT * FROM contacts ORDER BY id DESC")
    fun getAll(): LiveData<List<ContactEntity>>

    @Query("UPDATE contacts SET mode = :mode WHERE id = :id")
    fun updateMode(id: Int, mode: Double)
}