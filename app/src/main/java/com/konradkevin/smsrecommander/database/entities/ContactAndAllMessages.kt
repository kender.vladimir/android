package com.konradkevin.smsrecommander.database.entities

import androidx.room.Embedded
import androidx.room.Relation

class ContactAndAllMessages(
    @Embedded
    val contact: ContactEntity,

    @Relation(parentColumn = "id", entityColumn = "contactId", entity = MessageEntity::class)
    val messages: List<MessageEntity>
)