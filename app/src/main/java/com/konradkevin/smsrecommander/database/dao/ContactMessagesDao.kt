package com.konradkevin.smsrecommander.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.konradkevin.smsrecommander.database.entities.ContactAndAllMessages

@Dao
interface ContactMessagesDao {
    @Transaction
    @Query("SELECT * FROM contacts WHERE id = :contactId")
    fun getContactAndAllMessages(contactId: Int): LiveData<ContactAndAllMessages>

    @Transaction
    @Query("SELECT * FROM contacts")
    fun getAllContactsAndAllMessages(): LiveData<List<ContactAndAllMessages>>
}