package com.konradkevin.smsrecommander.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.konradkevin.smsrecommander.di.ViewModelFactory
import javax.inject.Inject

abstract class BaseFragment<M: ViewModel> constructor(@LayoutRes val layoutRes: Int, private val vmClass: Class<M>): Fragment() {

    @Inject lateinit var viewModelFactory: ViewModelFactory
    protected lateinit var viewModel: M

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()
        activity?.let {
            viewModel = ViewModelProviders.of(it, viewModelFactory).get(vmClass)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutRes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
        initObservers()
        initTransitions()
    }

    abstract fun inject()
    open fun initObservers() = Unit
    open fun initUi() = Unit
    open fun initTransitions() = Unit
}