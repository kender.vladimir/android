package com.konradkevin.smsrecommander.ui.main.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.konradkevin.smsrecommander.R
import com.konradkevin.smsrecommander.database.entities.ContactAndAllMessages
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.fragment_contacts_list_item.*
import java.text.DateFormat
import java.util.*

class ContactsListAdapter : ListAdapter<ContactAndAllMessages, ContactsListAdapter.ViewHolder>(DiffCallback()) {
    private lateinit var listener: OnItemClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.fragment_contacts_list_item, parent, false))
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        this.listener = listener
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position).let { contact ->
            holder.itemView.tag = contact
            holder.itemView.setOnClickListener { listener.onItemClick(contact) }
            holder.bind(contact)
        }
    }

    class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bind(contact: ContactAndAllMessages) {
            val lastMessageDate = if (contact.messages.isEmpty()) null else contact.messages.last().date
            var lastMessageDateString = ""

            if (lastMessageDate != null) {
                val calendar = Calendar.getInstance()
                calendar.timeInMillis = lastMessageDate
                lastMessageDateString = calendar.get(Calendar.HOUR_OF_DAY).toString() + ":" + calendar.get(Calendar.MINUTE).toString()
            }

            contacts_list_item_title.text = contact.contact.name
            contacts_list_item_message.text = if (contact.messages.isEmpty()) "" else contact.messages.last().content
            contacts_list_item_date.text = lastMessageDateString
        }
    }

    class DiffCallback : DiffUtil.ItemCallback<ContactAndAllMessages>() {
        override fun areItemsTheSame(oldItem: ContactAndAllMessages, newItem: ContactAndAllMessages) = oldItem.contact.id == newItem.contact.id
        override fun areContentsTheSame(oldItem: ContactAndAllMessages, newItem: ContactAndAllMessages) = oldItem == newItem
    }

    interface OnItemClickListener {
        fun onItemClick(contact: ContactAndAllMessages)
    }
}
