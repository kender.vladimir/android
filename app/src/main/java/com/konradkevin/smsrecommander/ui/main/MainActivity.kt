package com.konradkevin.smsrecommander.ui.main

import androidx.lifecycle.Observer
import com.konradkevin.smsrecommander.R
import com.konradkevin.smsrecommander.ui.base.BaseActivity
import com.konradkevin.smsrecommander.ui.main.details.ContactsDetailsFragment
import com.konradkevin.smsrecommander.ui.main.list.ContactsListFragment
import com.konradkevin.smsrecommander.utils.injector

class MainActivity : BaseActivity<MainViewModel>(R.layout.activity_main, MainViewModel::class.java) {

    override fun inject() {
        injector.inject(this)
    }

    override fun initObservers() {
        viewModel.contactId.observe(this, Observer {
            it?.let { goToContactsDetails() } ?: goToContactsList()
        })
    }

    private fun goToContactsList() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.main_container, ContactsListFragment.newInstance())
            .commit()
    }

    private fun goToContactsDetails() {
        supportFragmentManager.beginTransaction()
            .addToBackStack(null)
            .replace(R.id.main_container, ContactsDetailsFragment.newInstance())
            .commit()
    }
}
