package com.konradkevin.smsrecommander.ui.main.details

import android.widget.Button
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.konradkevin.smsrecommander.R
import com.konradkevin.smsrecommander.database.entities.RecommendationEntity
import com.konradkevin.smsrecommander.ui.base.BaseFragment
import com.konradkevin.smsrecommander.ui.main.MainViewModel
import com.konradkevin.smsrecommander.utils.injector
import kotlinx.android.synthetic.main.fragment_contacts_details.*

class ContactsDetailsFragment: BaseFragment<MainViewModel>(R.layout.fragment_contacts_details, MainViewModel::class.java) {

    private val messagesListAdapter = MessagesListAdapter()

    override fun inject() {
        injector.inject(this)
    }

    override fun initUi() {
        contacts_details_recycler_view.also {
            it.setHasFixedSize(true)
            it.layoutManager = LinearLayoutManager(context)
            it.adapter = messagesListAdapter
        }

        viewModel.setUserText("")

        contacts_details_edit_text.doOnTextChanged { text, _, _, _ ->
            viewModel.setUserText(text.toString())
        }

        suggestion1_btn.setOnClickListener {
            suggestionClick(suggestion1_btn)
        }

        suggestion2_btn.setOnClickListener {
            suggestionClick(suggestion2_btn)
        }

        suggestion3_btn.setOnClickListener {
            suggestionClick(suggestion3_btn)
        }

        contacts_details_btn_send.setOnClickListener {
            viewModel.addMessage(contacts_details_edit_text.text.toString())
            contacts_details_edit_text.setText("")
        }
    }

    override fun initObservers() {
        viewModel.contact.observe(viewLifecycleOwner, Observer {
            messagesListAdapter.submitList(it.messages)
            contacts_details_recycler_view.scrollToPosition(messagesListAdapter.itemCount - 1)
            showContactMode(it.contact.mode)
        })

        viewModel.recommendations.observe(viewLifecycleOwner, Observer {
            showRecommendations(it)
        })
    }

    private fun showContactMode(mode: Double?) {
        mode?.let { contacts_details_mode.text = String.format("%.3f", mode) }
    }

    private fun suggestionClick(button: Button) {
        if (contacts_details_edit_text.text.isNotEmpty() && contacts_details_edit_text.text.isNotBlank() && contacts_details_edit_text.text.last() == ' ') {
            val text = contacts_details_edit_text.text.toString()
            var rephrase = button.text.toString()

            if (text.length > 1 && text[text.length - 2] == '.') {
                rephrase = rephrase.capitalize()
            }

            contacts_details_edit_text.setText(getString(R.string.edit_text_value, contacts_details_edit_text.text.toString().capitalize(), rephrase, " "))
            contacts_details_edit_text.setSelection(contacts_details_edit_text.text.length)
        } else if (contacts_details_edit_text.text.isNotBlank() && contacts_details_edit_text.text.isNotEmpty()) {
            var rephrase = contacts_details_edit_text.text.split(' ').toMutableList()
            rephrase = rephrase.dropLast(1).toMutableList()
            if (rephrase.isNotEmpty() && rephrase.last().isNotEmpty() && rephrase.last().last() == '.') {
                rephrase.add(button.text.toString().capitalize())
            }
            else {
                rephrase.add(button.text.toString())
            }
            val finalString = rephrase.joinToString(separator = " ") { it }

            contacts_details_edit_text.setText(getString(R.string.edit_text_value, finalString.capitalize(), "", " "))
            contacts_details_edit_text.setSelection(contacts_details_edit_text.text.length)
        } else {
            contacts_details_edit_text.setText(getString(R.string.edit_text_value, "", button.text.toString().capitalize(), " "))
            contacts_details_edit_text.setSelection(contacts_details_edit_text.text.length)
        }
    }

    private fun showRecommendations(recommendations: List<RecommendationEntity>) {
        suggestion1_btn.text = if (recommendations.isNotEmpty()) recommendations[0].word1 else ""
        suggestion2_btn.text = if (recommendations.size > 1) recommendations[1].word1 else ""
        suggestion3_btn.text = if (recommendations.size > 2) recommendations[2].word1 else ""

        if (recommendations.isEmpty()) {
            var text = contacts_details_edit_text.text.toString()

            if (text.last() == ' ') {
                text = text.dropLast(1)
            }

            text += ". "
            contacts_details_edit_text.setText(text)
            contacts_details_edit_text.setSelection(contacts_details_edit_text.text.length)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = ContactsDetailsFragment()
    }
}