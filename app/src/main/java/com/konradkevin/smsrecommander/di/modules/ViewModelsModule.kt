package com.konradkevin.smsrecommander.di.modules

import androidx.lifecycle.ViewModel
import com.konradkevin.smsrecommander.database.dao.ContactDao
import com.konradkevin.smsrecommander.database.dao.ContactMessagesDao
import com.konradkevin.smsrecommander.database.dao.MessageDao
import com.konradkevin.smsrecommander.database.dao.RecommendationDao
import com.konradkevin.smsrecommander.di.ViewModelFactory
import kotlin.reflect.KClass
import com.konradkevin.smsrecommander.ui.main.MainViewModel
import dagger.*
import dagger.multibindings.IntoMap
import dagger.Provides
import javax.inject.Provider


@Module
object ViewModelsModule {

    @Target(AnnotationTarget.FUNCTION)
    @Retention(AnnotationRetention.RUNTIME)
    @MapKey
    internal annotation class ViewModelKey(val value: KClass<out ViewModel>)

    @Reusable
    @JvmStatic
    @Provides
    fun providesViewModelFactory(providerMap: MutableMap<Class<out ViewModel>, Provider<ViewModel>>): ViewModelFactory = ViewModelFactory(providerMap)

    @JvmStatic
    @Provides
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    fun providesMainViewModel(contactDao: ContactDao, contactMessagesDao: ContactMessagesDao, messageDao: MessageDao, recommendationDao: RecommendationDao): ViewModel = MainViewModel(contactDao, contactMessagesDao, messageDao, recommendationDao)
}