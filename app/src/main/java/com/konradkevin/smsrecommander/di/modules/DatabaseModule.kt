package com.konradkevin.smsrecommander.di.modules

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.framework.AssetSQLiteOpenHelperFactory
import com.konradkevin.smsrecommander.database.AppDatabase
import com.konradkevin.smsrecommander.database.dao.ContactDao
import com.konradkevin.smsrecommander.database.dao.ContactMessagesDao
import com.konradkevin.smsrecommander.database.dao.MessageDao
import com.konradkevin.smsrecommander.database.dao.RecommendationDao
import dagger.Module
import dagger.Provides
import dagger.Reusable
import javax.inject.Singleton

@Module
object DatabaseModule {

    @Singleton
    @JvmStatic
    @Provides
    fun provideAppDatabase(context: Context): AppDatabase {
        val database: RoomDatabase.Builder<AppDatabase> = Room.databaseBuilder(context, AppDatabase::class.java, "recommendations.db").also {
            it.allowMainThreadQueries()
        }

        return (database.openHelperFactory(AssetSQLiteOpenHelperFactory()).build())
    }

    @Reusable
    @JvmStatic
    @Provides
    fun provideContactDao(appDatabase: AppDatabase): ContactDao = appDatabase.contactDao()

    @Reusable
    @JvmStatic
    @Provides
    fun provideContactMessagesDao(appDatabase: AppDatabase): ContactMessagesDao = appDatabase.contactMessagesDao()

    @Reusable
    @JvmStatic
    @Provides
    fun provideMessageDao(appDatabase: AppDatabase): MessageDao = appDatabase.messageDao()

    @Reusable
    @JvmStatic
    @Provides
    fun provideRecommendationDao(appDatabase: AppDatabase): RecommendationDao = appDatabase.recommendationsDao()
}